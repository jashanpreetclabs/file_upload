var constant=require('./constant');

exports.somethingWentWrongError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.ERROR_IN_EXECUTION,
        data: {}
    }
    sendData(errResponse,res);
};

exports.successStatusMsg = function (res,data) {

    var successResponse = {
        status: constant.responseStatus.SHOW_DATA,
        message: constant.responseMessage.SUCCESSFUL_EXECUTION,
        data: data
    };
    sendData(successResponse,res);
};
exports.emptyFileUpload = function (res) {

    var errResponse = {
        status: constant.responseStatus.EMPTY_FILE,
        message: constant.responseMessage.EMPTY_FILE,
        data: {}
    };
    sendData(errResponse,res);
};

function sendData(data,res)
{
    res.type('json');
    res.json(data);
}
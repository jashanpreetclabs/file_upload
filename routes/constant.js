/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}

exports.responseStatus = {};


define(exports.responseStatus, "ERROR_IN_EXECUTION", 100);
define(exports.responseStatus, "SHOW_DATA", 101);
define(exports.responseStatus, "EMPTY_FILE", 102);

exports.responseMessage = {}; 

define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SUCCESSFUL_EXECUTION", "Upload Successful.");
define(exports.responseMessage, "EMPTY_FILE", "EMPTY FILE CANNOT BE UPLOADED.");



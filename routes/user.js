var sendResponse = require('./sendResponse');
var func = require('./commonFunction');
var request = require("request");
var express = require('express');
var router = express.Router();

router.post('/userImage',function(req,res){
    var currentTime = new Date().getTime().toString();
    req.files.user_image.name=currentTime;
    func.imageUploadOnServer(req.files.user_image,'./Images/',function (result) {
        if(result === 0){
            sendResponse.somethingWentWrongError(res);
        }
        else if (result === 1){
            sendResponse.emptyFileUpload(res);
        }
        else {
            sendResponse.successStatusMsg(res,result);
        }
    });

});

module.exports = router;
process.env.NODE_ENV = 'localDevelopment';
var express = require("express");
var http = require('http');
var path = require('path');
var config=require('config');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');

var multipartMiddleware = multipart();
var app = express();


var routes = require('./routes/user');
var index=require('./routes/index');


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/',index);
app.post('/userImage', multipartMiddleware);
app.post('/userImage', routes);


http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});
